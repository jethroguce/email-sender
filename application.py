import json
from mail import Mail


def send_mail(**kwargs):
    mail = Mail(**kwargs)
    return mail.send()


def handler(event, context):
    message = event['body-json']
    print(message)
    if message.get('type', None) == 'mail':
        status, description = send_mail(**message)


if __name__ == '__main__':
    handler(None, None)
