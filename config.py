import os

MAIL_SERVER = os.environ.get('MAIL_SERVER', None)
MAIL_USERNAME = os.environ.get('MAIL_USERNAME', None)
MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD', None)
MAIL_PORT = os.environ.get('MAIL_PORT', None)
MAIL_USE_SSL = os.environ.get('MAIL_SSL', None)
MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER', None)
