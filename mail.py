import config
from flask import Flask
from flask_mail import Mailer, Message


class Mail(object):
    def __init__(self,
                 subject,
                 sender,
                 recipient,
                 body,
                 body_html=None,
                 to=None,
                 extra_headers=None,
                 **kwargs):
        print('initializing mail')
        self.__app = Flask(__name__)
        self.__app.config.from_object('config')

        self.__mailer = Mailer(self.__app)

        self.__msg = Message(subject,
                             body=body,
                             html=body_html,
                             sender=sender,
                             recipients=recipient,
                             extra_headers=extra_headers
                             )

    def send(self):
        with self.__app.app_context():
            print('sending mail')
            self.__mailer.send(self.__msg)

            return None, None
